# README #

### NAMEN REPOZITORIJA ###

Repozitorij je za igro mlin za ali dva igralca ali pa igranje proti računalniku.
igra je bila narejena za predmet Programiranje 2.

### PRAVILA IGRE ###

Mlin je igra za dva igralca, belega, ki začne, in črnega, vsak s po devet žetoni.

Cilj igre je postavljati "mline", tj. tri žetone postaviti v vrsto, pri čemer lahko nasprotniku vzameš en žeton. Jemanje iz nasprotnikovega mlina je dovoljeno le, če ni nobenega drugega na voljo.

V prvem delu igre igralca izmenično postavljata žetone na prosta polja. Ko oba postavite vse, se začne naslednji del. 

Žetone se zdaj premika na sosednja prosta polja po narisanih povezavah. Če igralec ne more premakniti nobenega žetona ali ostane s samo dvema, izgubi. 
Če ima manj kot tri žetone, lahko "skače" in jih prestavlja na poljubno prosto polje. 
Če igralec naredi mlin s premikom žetona iz že prej obstoječega mlina, lahko vzame nasprotniku kar dva žetona. 

### UPORABA ###

Igra se začne, ko zaženete `Mlin.py`. 
V menuju `File` `Nova igra` nastavi ploščo za novo igro, pod `options` pa lahko izberete nasprotnika, kar tudi na zažene igro. (Enega od igralcev mora igrati uporabnik.)

Igra se z miško. Levi klik na polje postavi ali pobere ali izbriše žeton. Desni klik spusti žeton, ki se premika, na začetno polje brez posledic.

Opozorilo: včasih računalnik postavi žeton na tisto polje, iz katerega je bil prejšnji odstranjen, zato lahko zgleda, kot da se ni nič zgodilo.

### Kontakt ###

Avtor: matic-oskar.hajsen@student.fmf.uni-lj.si