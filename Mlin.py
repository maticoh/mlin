from tkinter import *
from classes import *
from ai import *


def koord(x,y):
    return (x*100-50,y*100-50)

BELA = "bela"
CRNA = "crna"
      
                


class Aplikacija():

    def __init__(self, master,
                 barva_polja="light yellow",
                 barva_crt="grey"):
        self.plosca = Plosca()
        plosca = self.plosca
        plosca.naredi_povezave()
        self.radij = 20
        self.zmagovalec = None      

######### Začetne nastavitve ##############################################
        
        self.se_igra = True

        self.racunalnik = {BELA: False, CRNA: True} 
        
        self.poteza = BELA
        self.faza = 1 # faza 1: postavljanje žetonov na ploščo, faza 2: premikanje žetonov

        self.mlin = 0 # ob navadnem mlinu postane 1, ob dvojnem 2
        self.super_mlin = False # ali lahko brišemo tudi žetone v mlinu
        self.premikaj = False # ali premikamo žeton
        self.prejsnji_zeton = None # položaj žetona, ki ga premikamo in se uporablja v nekaterih funkcijah
        self.narisi = True # ali pri premikanju nariše pomožni žeton
        self.combo = False # si zapomni, ali je dvojni mlin
        
        self.faza_1_bela = 9 # koliko jih je posamezni igralec postavil v 1. fazi
        self.faza_1_crna = 9

        self.stevilo_zetonov_bela = 0 # koliko žetonov je ostalo posameznemu igralcu
        self.stevilo_zetonov_crna = 0
        self.skace = set() # kdo lahko premika žetone ne glede na povezave, ker ima le še 3 žetone

######### Začetek platna #########################################################

        self.platno = Canvas(master, width=700, height=700, bg=barva_polja)
        self.platno.grid(row=0, column=0, rowspan=7, columnspan=7)
        platno = self.platno
        platno.bind("<Button-1>", self.klik)
        platno.bind("<Button-3>", self.spusti)
        platno.bind("<Motion>", self.premikaj_zeton)

######### Okvirji s statistiko ############################################

        # Statistika
        okvir_poteza = LabelFrame(master, text="Na potezi:", font="bold")
        okvir_poteza.grid(row=0, column=7)
        okvir_beli = LabelFrame(master, text="Beli igralec", font="bold")
        okvir_beli.grid(row=1, column=7)
        okvir_crni = LabelFrame(master, text="Črni igralec", font="bold")
        okvir_crni.grid(row=2, column=7)

#---------- začetni stringi -----------#
        
        self.statistika_beli = StringVar()
        self.statistika_beli.set(9)
        
        self.zmage_beli = StringVar()
        self.stevec_zmag_beli = 0 
        self.zmage_beli.set(str(self.stevec_zmag_beli))
        self.stevec_zmag_crni = 0 
        self.zmage_crni = StringVar()
        self.zmage_crni.set(str(self.stevec_zmag_crni))
        
        self.statistika_crni = StringVar()
        self.statistika_crni.set(9)
        self.navodilo = StringVar()
        self.navodilo.set("Položi še:")
        self.poteza_txt = StringVar()
        self.poteza_txt.set("BELI IGRALEC")
        self.dejanje_txt = StringVar()
        self.dejanje_txt.set("Postavi žeton")
        stat_poteza = Label(okvir_poteza, textvariable=self.poteza_txt).grid(row=0, column=0)

#---------- napis računalnika -----------#
        
        self.stat_beli_ai = StringVar()
        self.stat_crni_ai = StringVar()
        if self.racunalnik[BELA]:
            self.stat_beli_ai.set("Računalnik")
        else:
            self.stat_beli_ai.set("Uporabnik")
        if self.racunalnik[CRNA]:
            self.stat_crni_ai.set("Računalnik")
        else:
            self.stat_crni_ai.set("Uporabnik")

#---------- prikaz labelov -----------#
        
        stat_dejanje = Label(okvir_poteza, textvariable=self.dejanje_txt).grid(row=1, column=0)
        stat_txt_beli = Label(okvir_beli, textvariable=self.navodilo, justify=LEFT).grid(row=0, column=0)
        stat_st_beli = Label(okvir_beli, textvariable=self.statistika_beli, justify=LEFT).grid(row=0, column=1)
        stat_txt_zmage_beli = Label(okvir_beli, text="Zmage:", justify=LEFT).grid(row=1, column=0)
        stat_zmage_beli = Label(okvir_beli, textvariable=self.zmage_beli, justify=LEFT).grid(row=1, column=1)

        stat_beli_ai_label= Label(okvir_beli, textvariable=self.stat_beli_ai, justify=RIGHT).grid(row=2, column=0)
        stat_crni_ai_label= Label(okvir_crni, textvariable=self.stat_crni_ai, justify=RIGHT).grid(row=2, column=0)
        
        stat_txt_crni = Label(okvir_crni, textvariable=self.navodilo, justify=LEFT).grid(row=0, column=0)
        stat_st_crni = Label(okvir_crni, textvariable=self.statistika_crni, justify=LEFT).grid(row=0, column=1)
        stat_txt_zmage_crni = Label(okvir_crni, text="Zmage:", justify=LEFT).grid(row=1, column=0)
        stat_zmage_crni = Label(okvir_crni, textvariable=self.zmage_crni, justify=LEFT).grid(row=1, column=1)

############## Menu ###########################################################3

        # Menu
        menubar = Menu(master)

        filemenu = Menu(menubar)
        filemenu.add_command(label="Nova igra", command=self.nova_igra)
        menubar.add_cascade(label="File", menu=filemenu)

        optionmenu = Menu(menubar)
        optionmenu.add_command(label="Beli igralec", command=self.beli_igralec)
        optionmenu.add_command(label="Beli računalnik", command=self.beli_racunalnik)
        optionmenu.add_command(label="Črni igralec", command=self.crni_igralec)
        optionmenu.add_command(label="Črni računalnik", command=self.crni_racunalnik)
        menubar.add_cascade(label="Options", menu=optionmenu)                               

        master.config(menu=menubar)
        
        # Nariše igralno ploščo: polja, na katere lahko položimo žetone in črte, po katerih lahko vlečemo.

############## Izris plošče na platno ###########################################

        # Krogi
        for p in plosca.polozaji:
            self.narisi_krog(p, barva_crt, 25, platno)
                               

        # Črte     
        for i in range(1,4):
            platno.create_rectangle(koord(i,i)[0],koord(i,i)[1],
                                    koord(8-i,8-i)[0],koord(8-i,8-i)[1],
                                    outline= barva_crt,
                                    width = 5)
        platno.create_line(koord(4,1)[0],koord(4,1)[1],
                           koord(4,3)[0],koord(4,3)[1],
                           fill = barva_crt,
                           width = 5)
        platno.create_line(koord(4,5)[0],koord(4,5)[1],
                           koord(4,7)[0],koord(4,7)[1],
                           fill = barva_crt,
                           width = 5)
        platno.create_line(koord(1,4)[0],koord(1,4)[1],
                           koord(3,4)[0],koord(3,4)[1],
                           fill = barva_crt,
                           width = 5)
        platno.create_line(koord(5,4)[0],koord(5,4)[1],
                           koord(7,4)[0],koord(7,4)[1],
                           fill = barva_crt,
                           width = 5)

############## Če računalnik igra prvo potezo ####################################


        if self.racunalnik[self.poteza]:
            self.igraj_rac()
            self.spremeni_potezo()


############## Pomožne funkcije #####################################################

    def narisi_krog(self, poz, barva, d, master): # za začetna polja na plošči
        master.create_oval(koord(poz[0],poz[1])[0]-d,
                           koord(poz[0],poz[1])[1]-d,
                           koord(poz[0],poz[1])[0]+d,
                           koord(poz[0],poz[1])[1]+d,
                           outline= barva,
                           fill = barva)
    
      
    def nova_igra(self):
        '''Začne novo igro in ponastavi podatke na začetne vrednosti'''
        for p in self.plosca.polja:
            zet = self.plosca.polja[p]
            zet.barva = None
            self.platno.delete(zet.zeton)

        self.se_igra = True
        if self.zmagovalec != None:
            self.platno.delete(self.zmagovalec)
        self.zmagovalec = None
        self.poteza = BELA
        self.faza = 1

        self.mlin = 0
        self.super_mlin = False
        self.premikaj = False
        self.prejsnji_zeton = None
        self.narisi = True
        self.combo = False
        
        self.faza_1_bela = 9
        self.faza_1_crna = 9

        self.stevilo_zetonov_bela = 0
        self.stevilo_zetonov_crna = 0
        self.skace = set()
        self.statistika_beli.set(9)
        self.statistika_crni.set(9)

        self.navodilo.set("Položi še:")

        if self.racunalnik[self.poteza]:
            self.igraj_rac()
            self.spremeni_potezo()

        
    def kam(self, x,y):
        '''Določi, na katero polje je igralec kliknil.'''
        for k in self.plosca.polja:
            s = koord(k[0],k[1])
            a = s[0]
            b = s[1]
            if (x - a)**2 + (y-b)**2 <= 600:
                return k
        return None


    def risi(self, poz, barva):
        '''Osnovna funkcija za izris žetonov obeh igralcev.'''
        if barva == BELA:
            return self.platno.create_oval(poz[0]-self.radij,
                                           poz[1]-self.radij,
                                           poz[0]+self.radij,
                                           poz[1]+self.radij,
                                           fill = "white")
        elif barva == CRNA:
            return self.platno.create_oval(poz[0]-self.radij,
                                           poz[1]-self.radij,
                                           poz[0]+self.radij,
                                           poz[1]+self.radij,
                                           
                                           fill = "black",
                                           outline = "white")


########################## Konec poteze ###########################################


    def spremeni_potezo(self):
        '''Na koncu poteze določi, v kateri fazi je igra, in zamenja igralca.'''
        if self.poteza == BELA:
            self.poteza = CRNA
            self.poteza_txt.set("ČRNI IGRALEC")
            if self.faza == 1:
                self.faza_1_bela -= 1
                self.statistika_beli.set(str(self.faza_1_bela))
                self.dejanje_txt.set("Postavi žeton")
            else:
                self.statistika_beli.set(str(self.stevilo_zetonov_bela))
        else:
            self.poteza = BELA
            self.poteza_txt.set("BELI IGRALEC")
            if self.faza == 1:
                self.faza_1_crna -= 1
                self.statistika_crni.set(str(self.faza_1_crna))
                self.dejanje_txt.set("Postavi žeton")       
            else:
                self.statistika_beli.set(str(self.stevilo_zetonov_bela))
                
        if self.faza_1_crna == 0 and self.faza_1_bela == 0 and self.faza == 1:
            self.faza = 2
            self.navodilo.set("V posesti:")
            self.statistika_beli.set(str(self.stevilo_zetonov_bela))
            self.statistika_crni.set(str(self.stevilo_zetonov_crna))
            self.dejanje_txt.set("Premakni žeton")
        elif self.faza == 2:
            self.dejanje_txt.set("Premakni žeton")
            if self.stevilo_zetonov_bela <= 3:
                self.skace.add(BELA)
            if self.stevilo_zetonov_crna <= 3:
                self.skace.add(CRNA)
        self.prejsnji_zeton = None
        self.combo = False
        if self.faza == 2 and self.se_igra:
            self.razglasi_zmagovalca(self.preveri_konec())


    def preveri_konec(self):
        '''Preveri, ali je igre konec. Pogoji: manj kot 2 žetona, brez možne poteze'''
        if BELA in self.skace:
            i = 100
        else:
            i = 0
        if CRNA in self.skace:
            j = 100
        else:
            j = 0
        for zet in self.plosca.polja:
            if self.plosca.polja[zet].barva == BELA and self.plosca.polja[zet].prost():
                i += 1
            elif self.plosca.polja[zet].barva == CRNA and self.plosca.polja[zet].prost():
                j += 1
        if self.stevilo_zetonov_crna <= 2 or j == 0:
            return BELA

        elif self.stevilo_zetonov_bela <= 2 or i == 0:
            return CRNA

        else:
            return False
        
        
    def razglasi_zmagovalca(self, barva):
        '''Požene "preveri_konec()" in ustavi igro ter določi zmagovalca, če je res.'''
        if barva == False:
            pass
        elif barva == BELA:
            self.se_igra = False
            self.zmagovalec = self.platno.create_text(350,
                                                      350,
                                                      text="Beli je zmagal!",
                                                      font=10)
            self.stevec_zmag_beli += 1
            self.zmage_beli.set(str(self.stevec_zmag_beli))
        elif barva == CRNA:
            self.se_igra = False
            self.zmagovalec = self.platno.create_text(350,
                                                      350,
                                                      text="Črni je zmagal!",
                                                      font=10)
            self.stevec_zmag_crni += 1
            self.zmage_crni.set(str(self.stevec_zmag_crni))
                

#################### Računalnikove poteze ##########################################


    def igraj_rac(self):
        '''Požene računalnikove poteze, odvisno od faze igre.'''
        if self.se_igra:
            if self.faza == 1:
                self.igraj_rac_1()
            else:
                self.igraj_rac_2()


    def igraj_rac_1(self):
        '''Postvlja žetone v fazi 1 in jih jemlje ob mlinu.'''
        if self.poteza == BELA:
            b = BELA
        else:
            b = CRNA
        pamet = Minimax(self, b)
        zet = pamet.igraj()
        self.narisi_zeton(zet)
        if zet.preveri_mlin():
            izbor = self.izberi_zeton_rac()
            self.brisi_zeton(izbor)
        self.mlin = 0
            

    def igraj_rac_2(self):
        '''Premika žetone v fazi 2 in jih jemlje ob mlinu.'''
        if self.poteza == BELA:
            b = BELA
        else:
            b = CRNA
        pamet = Minimax(self, b)
        izbira = pamet.igraj()
        zet = izbira
        zet_prej = self.najdi_zeton(zet)
        self.premakni_zeton(zet_prej)
        self.narisi_zeton(zet)
        zdaj = zet.preveri_mlin()
        prej = self.combo
        if prej and zdaj:
            self.mlin = 2
            self.dejanje_txt.set("Odstrani žeton")
        elif zdaj:
            self.mlin = 1
            self.dejanje_txt.set("Odstrani žeton")
        while self.mlin > 0:
            izbor = self.izberi_zeton_rac()
            self.brisi_zeton(izbor)


    def najdi_zeton(self, zet):
        if self.poteza in self.skace:
            for x in self.plosca.polja:
                if self.plosca.polja[x].barva == self.poteza:
                    return self.plosca.polja[x]
        else:
            for x in zet.povezave:
                if x.barva == self.poteza:
                    return x


    def izberi_zeton_rac(self):
        '''Najde žeton, ki ga vzame računalnik, ko doseže mlin.'''
        if self.poteza == BELA:
            b = CRNA
            c = BELA
        else:
            b = BELA
            c = CRNA
        n = 0
        zet = None
        moznosti = self.plosca.poteze(BELA, CRNA)[b]
        lahko_vse = self.samo_mlin(c)
        for x in moznosti:
            if not x[0].preveri_mlin() or lahko_vse:
                ocena = x[0].oceni_vred_v_mlinu_1()
                if ocena >= n:
                    zet = x[0]
        return zet


######################### Glavna funkcija, s katere so klicane druge #############################


    def klik(self, event):
        '''Glede na stanje na plošči stori, kar igralec mora s klikom na levi gumb miške:
           postavlja žetone, jih premika ali odstranjuje.'''
        t = self.kam(event.x,event.y)
        if t != None and self.se_igra:
            zet = self.plosca.polja[t]
            # odstranjevanje nasprotnega žetona, zaradi mlina
            if self.mlin > 0 and zet.barva != self.poteza and zet.barva != None: # preveri, če smo v mlinu (1 za navadni, 2 za dvojni)
                self.brisi_zeton(zet)
                if self.mlin == 0:
                    self.spremeni_potezo()
                    if self.racunalnik[self.poteza]:
                        self.igraj_rac()
                        self.spremeni_potezo()
            # postavljanje prvih žetonov
            elif self.faza == 1 and zet.barva == None and self.mlin == 0:
                self.narisi_zeton(zet) # nariše žeton na prazno polje
                if zet.preveri_mlin():
                    self.mlin = 1
                    self.dejanje_txt.set("Odstrani žeton")
                else:
                    self.spremeni_potezo()
                    if self.racunalnik[self.poteza]:
                        self.igraj_rac()
                        self.spremeni_potezo()
            # premikanje žetonov
            elif self.faza == 2 and self.mlin == 0:
                if self.premikaj:
                    if self.prejsnji_zeton != zet and (zet in self.prejsnji_zeton.povezave or self.poteza in self.skace) and self.premikaj and zet.barva == None and (self.prejsnji_zeton in zet.povezave or self.poteza in self.skace) and self.prejsnji_zeton != zet:
                        self.narisi_zeton(zet)
                        self.prejsnji_zeton = None
                        zdaj = zet.preveri_mlin()
                        prej = self.combo
                        if prej and zdaj:
                            self.mlin = 2
                            self.dejanje_txt.set("Odstrani žeton")
                        elif zdaj:
                            self.mlin = 1
                            self.dejanje_txt.set("Odstrani žeton")
                        if self.mlin == 0:
                            self.spremeni_potezo()
                            if self.racunalnik[self.poteza]:
                                self.igraj_rac_2()
                                self.spremeni_potezo()
                elif zet.barva == self.poteza:
                    i = 0
                    for x in zet.povezave: 
                        if x.barva == None:
                            i += 1
                    if i > 0 or self.poteza in self.skace:
                        self.premakni_zeton(zet)


################# Postavljanje žetonov #################################                

                
    def narisi_zeton(self, zet):
        '''Funkcija, ki jo kliče funkcija "klik", če je potrebno narisati žeton.
           Preveri pogoje in če so ustrezni, nariše žeton s funkcijo "risi,"'''
        # Faza 2. Če ima igralec 3 žetone, jih lahko postavi, kamor želi.
        if self.faza == 2:
            if self.premikaj and zet.barva == None and (self.prejsnji_zeton in zet.povezave or self.poteza in self.skace) and self.prejsnji_zeton != zet:
                poz = koord(zet.x,
                            zet.y)
                self.premikaj = False
                if self.poteza == BELA:
                    zet.barva = BELA
                    zet.zeton = self.risi(poz, BELA)
                    self.platno.delete(self.premicni_beli)
                    
                else:
                    zet.barva = CRNA
                    zet.zeton = self.risi(poz, CRNA)
                    self.platno.delete(self.premicni_crni)

        # Faza 1
        elif self.faza == 1:
            if zet.barva == None:
                poz = koord(zet.x,
                            zet.y)
                if self.poteza == BELA:
                    zet.barva = BELA
                    zet.zeton = self.risi(poz, BELA)
                    self.stevilo_zetonov_bela += 1
                else:
                    zet.barva = CRNA
                    zet.zeton = self.risi(poz, CRNA)
                    self.stevilo_zetonov_crna += 1
            
            
    def brisi_zeton(self, zet):
        '''Ko igralcu uspe narediti mlin, "klik" kliče to funkcijo za izbris nasprotnikovega žetona.'''
        if zet.barva != self.poteza and zet.barva != None: # ali smo sploh izbrali nasprotnikov žeton
            if not zet.preveri_mlin(): # preveri, ali je žeton v mlinu in ga v vsakem primeru izbriše, če ni
                self.platno.delete(zet.zeton)
                zet.barva = None
                if self.poteza == BELA:
                    self.stevilo_zetonov_crna -= 1
                    if self.faza == 2: self.statistika_crni.set(str(self.stevilo_zetonov_crna))
                else:
                    self.stevilo_zetonov_bela -= 1
                    if self.faza == 2: self.statistika_beli.set(str(self.stevilo_zetonov_bela))
                self.mlin -= 1
            else: # če ni, obstaja možnost, da ima vse v mlinu, kar pomeni, da jih tudi lahko jemljemo.
                if self.samo_mlin(self.poteza): # če jih nima, izbranega izbriše
                    self.platno.delete(zet.zeton)
                    zet.barva = None
                    if self.poteza == BELA:
                        self.stevilo_zetonov_crna -= 1
                    elif self.poteza == CRNA:
                        self.stevilo_zetonov_bela -= 1
                    self.mlin -= 1

            
    def samo_mlin(self, barva):
        i = 0
        for a in self.plosca.polja: #preveri, ali ima nasprotnik proste žetone
            if self.plosca.polja[a].barva != barva and self.plosca.polja[a].barva != None:
                if self.plosca.polja[a].preveri_mlin() == False:
                    i += 1
        return i == 0
            
    
    def premakni_zeton(self, zet):
        '''Ko sta igralca postavila vse svoje žetone, "klik" uporablja to funkcijo za premik žetonov.
           Nariše pomožni žeton, ki se premika in izbriše prvotnega.'''
        if zet.barva == self.poteza:
            poz = koord(zet.x,
                        zet.y)
            self.prejsnji_zeton = zet
            self.combo = zet.preveri_mlin()
            if zet.barva == BELA:
                self.premicni_beli = self.risi(poz, BELA)
            else:
                self.premicni_crni = self.risi(poz, CRNA)
            self.platno.delete(zet.zeton)
            zet.barva = None
            self.premikaj = True
            self.dejanje_txt.set("Postavi žeton")

        
    def premikaj_zeton(self, event):
        '''Premika pomožni žeton, narisan s "premakni_zeton".'''
        if self.premikaj:
            r = self.radij
            (x,y) = (event.x, event.y)
            if self.poteza == BELA:
                self.platno.coords(self.premicni_beli, x-r, y-r, x+r, y+r)
            else:
                self.platno.coords(self.premicni_crni, x-r, y-r, x+r, y+r)


    def spusti(self, event):
        '''Če igralec ni zadovoljen z izbiro žetona za premikanje, ga lahko z desnim klikom spusti na prvotno mesto.'''
        if self.premikaj:
            self.premikaj = False
            zet = self.plosca.polja[(self.prejsnji_zeton.x,
                                     self.prejsnji_zeton.y)]
            poz = koord(self.prejsnji_zeton.x,
                        self.prejsnji_zeton.y)
            zet.barva = self.poteza
            if self.poteza == BELA:
                zet.zeton = self.risi(poz, BELA)
                self.platno.delete(self.premicni_beli)
            else:
                zet.zeton = self.risi(poz, CRNA)
                self.platno.delete(self.premicni_crni)
            self.prejsnji_zeton = None
            self.dejanje_txt.set("Premakni žeton")
    

########### Menjava igralcev ######################################################

    def beli_racunalnik(self):
        '''Zamenja upravitelja te barve in zažene novo igro. Če je drugi tudi računalnik, zamenja tudi njega.'''
        self.racunalnik[CRNA] = False
        self.racunalnik[BELA] = True
        if self.racunalnik[self.poteza]:
            self.igraj_rac()
        if self.racunalnik[BELA]:
            self.stat_beli_ai.set("Računalnik")
        else:
            self.stat_beli_ai.set("Uporabnik")
        if self.racunalnik[CRNA]:
            self.stat_crni_ai.set("Računalnik")
        else:
            self.stat_crni_ai.set("Uporabnik")
        self.nova_igra()


    def beli_igralec(self):
        '''Zamenja upravitelja te barve in zažene novo igro. Vselej je vsaj en igralec.'''
        self.racunalnik[BELA] = False
        if self.racunalnik[BELA]:
            self.stat_beli_ai.set("Računalnik")
        else:
            self.stat_beli_ai.set("Uporabnik")
        if self.racunalnik[CRNA]:
            self.stat_crni_ai.set("Računalnik")
        else:
            self.stat_crni_ai.set("Uporabnik")
        self.nova_igra()


    def crni_racunalnik(self):
        '''Zamenja upravitelja te barve in zažene novo igro. Če je drugi tudi računalnik, zamenja tudi njega.'''
        self.racunalnik[BELA] = False
        self.racunalnik[CRNA] = True
        if self.racunalnik[self.poteza]:
            self.igraj_rac()
        if self.racunalnik[BELA]:
            self.stat_beli_ai.set("Računalnik")
        else:
            self.stat_beli_ai.set("Uporabnik")
        if self.racunalnik[CRNA]:
            self.stat_crni_ai.set("Računalnik")
        else:
            self.stat_crni_ai.set("Uporabnik")
        self.nova_igra()


    def crni_igralec(self):
        '''Zamenja upravitelja te barve in zažene novo igro. Vselej je vsaj en igralec.'''
        self.racunalnik[CRNA] = False
        if self.racunalnik[BELA]:
            self.stat_beli_ai.set("Računalnik")
        else:
            self.stat_beli_ai.set("Uporabnik")
        if self.racunalnik[CRNA]:
            self.stat_crni_ai.set("Računalnik")
        else:
            self.stat_crni_ai.set("Uporabnik")
        self.nova_igra()
        
        
                    

root = Tk()
App = Aplikacija(root)
root.mainloop()

