from classes import *


BELA = "bela"
CRNA = "crna"


                            
class Minimax():

    def __init__(self, app, barva, globina = 2):
        # Dela za globino 1 in 2.
        self.app = app
        self.plosca = app.plosca
        self.globina = globina
        self.prosti = self.app.plosca.poteze(BELA, CRNA)[None]
        self.barva = barva


    def igraj(self):
        '''Kliče nadaljnje funkcije za določitev najboljše poteze.'''
        (p, vrednost_p) = self.minimax(self.globina, None, -10000)
        return p


    def naredi_potezo_1(self, zet):
        '''Simulira potezo na tisto polje.'''
        if self.app.poteza == BELA:
            zet.barva = BELA
        elif self.app.poteza == CRNA:
            zet.barva = CRNA


    def razveljavi_potezo_1(self, zet):
        '''Razveljavi simuliratno potezo.'''
        zet.barva = None


    def zamenjaj_potezo_1(self):
        '''Zamenja potezo naslednjemu igralcu.'''
        if self.app.poteza == BELA:
            self.app.poteza = CRNA
        elif self.app.poteza == CRNA:
            self.app.poteza = BELA


    def mozne_poteze(self):
        '''Glede na fazo določi možne poteze. Vrača jih v paru z izvornim žetonom.'''
        if self.app.faza == 1: # Vsa prazna polja in None kot izvor.
            return self.app.plosca.poteze(BELA, CRNA)[None]
        elif self.app.faza == 2 and self.app.poteza not in self.app.skace: # Vse proste povezave izvornega žetona
            s = []
            for x in self.app.plosca.poteze(BELA, CRNA)[self.app.poteza]:
                mozne = x[0].proste_vezave()
                for y in mozne:
                    s.append((y, x[0]))
            return s
        else: # vsa prazna polja in žeton, ki lahko skoči nanj
            s = []
            mozne = self.app.plosca.poteze(BELA, CRNA)[None]
            for x in self.app.plosca.poteze(BELA, CRNA)[self.app.poteza]:
                for y in mozne:
                    s.append((y[0], x[0]))
            return s
            
    
    def minimax(self, globina, pot, vre):
        '''Deluje pri globini 2.'''
        if globina == 0:
            v = pot.oceni_potezo(self.app.faza)
            return (pot, v)
        else:
            self.prosti = self.mozne_poteze() 
            pot = None
            vre = None
            for x in self.prosti:
                poteza = x[0]
                prej = x[1]
                if x[1] != None:
                    x[1].barva = None
                self.naredi_potezo_1(poteza)
                t_vre = poteza.oceni_potezo(self.app.faza)
                if globina > 1:
                    self.zamenjaj_potezo_1()
                (q, vrednost_q) = self.minimax(globina - 1, poteza, vre)
                self.razveljavi_potezo_1(poteza)
                if globina > 1:
                    self.zamenjaj_potezo_1()
                if x[1] != None:
                    x[1].barva = self.app.poteza
                if self.app.poteza != self.barva:
                    vrednost_q = -vrednost_q
                t_vre += vrednost_q
                if (vre == None) or t_vre > vre:
                    vre = t_vre
                    pot = poteza
            return (pot, vre)

            
































    


        
