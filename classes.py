from copy import deepcopy
from random import *

polozaji = {(1,1),(1,4),(1,7),
            (2,2),(2,4),(2,6),
            (3,3),(3,4),(3,5),
            (4,1),(4,2),(4,3),
            (4,5),(4,6),(4,7),
            (5,3),(5,4),(5,5),
            (6,2),(6,4),(6,6),
            (7,1),(7,4),(7,7)}


vrednost_mlin = 100 # žeton v mlinu 
vrednost_blok = 10 # žeton, ki blokira mlin
vrednost_prost = 0
vrednost_nas = 0
vrednost_napad = 0

class Polje:
    
    def __init__(self, polozaj):
        self.x = polozaj[0]
        self.y = polozaj[1]
        self.polozaj = (self.x, self.y)
        self.povezave = []
        self.barva = None
        self.zeton = None
        self.mlin = []
        self.vrednost = 0 # gleda, kateri je najpomembnejši pri hevristiki


    def __repr__(self):
        return "Polje na " + str(self.polozaj)


    def povezi(self,other):
        '''Naredi povezave do drugih polj.'''
        if other != None:
            self.povezave.append(other)
            other.povezave.append(self)
        else:
            pass


    def vezave_koord(self):
        '''Vrne koordinate povezenih polj.'''
        v = []
        for u in self.povezave:
            v.append((u.x,u.y))
        return v


    def proste_vezave(self):
        '''Vrne seznam prostih sosednjih polj.'''
        v = []
        for u in self.povezave:
            if u.barva == None and u not in v:
                v.append(u)
        return v 


    def preveri_mlin_vrednost(self):
        '''Vrne, v koliko mlinih se trenutno nahaja.'''
        i = 0
        for x in self.mlin:
            if x.preveri_barvo(self.barva)[0] == 3:
                i += 1
        return i


    def preveri_mlin(self):
        '''Ali je v mlinu.'''
        return self.preveri_mlin_vrednost() > 0

    
    def prost(self):
        '''Ali se lahko premakne.'''
        return len(self.proste_vezave())>0

                
    def vrednoti_povezave(self, barva):
        '''Za oceno vredosti polja - premičnost.'''
        n = 0
        for vez in self.povezave:
            if vez.barva == None:
                n += vrednost_prost
            elif vez.barva == barva:
                n += vrednost_nas
        return n


    def vrednoti_napad(self, barva):
        '''Za oceno vredosti polja - možnost za nastanek mlina.'''
        n = 0
        for vez in self.proste_vezave():
            for m in vez.mlin:
                if m.preveri_barvo(barva)[0] == 2:
                    n += vrednost_napad
        return n
    

    def lahko_naredi_mlin(self):
        i = 0
        for sosed in self.proste_vezave():
            for smer in sosed.mlin:
                (dobri_2, prosti_2, slabi_2) = smer.preveri_barvo(self.barva)
                if prosti_2 == 1 and dobri_2 == 2:
                    i += 1
        return i


    def oceni_vred_v_mlinu_1(self):
        n = 0
        for smer in self.mlin:
            (dobri, prosti, slabi) = smer.preveri_barvo(self.barva)
            if dobri == 2:
                n += vrednost_blok
            elif slabi == 2:
                n += vrednost_napad
            n += self.vrednoti_povezave(self.barva)
        return n


    def oceni_potezo(self, faza):
        n = 0
        b = self.barva
        if self.preveri_mlin():
            n += vrednost_mlin
        for smer in self.mlin:
            (dobri, prosti, slabi) = smer.preveri_barvo(self.barva)
            if slabi == 2:
                n += vrednost_blok
            elif dobri == 2 and prosti == 1:
                n += vrednost_napad
        if faza == 2: n += self.lahko_naredi_mlin() * vrednost_napad
        n += self.vrednoti_povezave(self.barva)
        return n


class Mlin:

    def __init__(self, vrsta):
        self.vrsta = vrsta

    
    def __repr__(self):
        return "Mlin z " + str(self.vrsta)


    def preveri_barvo(self, barva):
        '''Vrne, koliko dobrih, koliko prostih in koliko slabih.
        (naši, prosti, nasprotnikovi)'''
        i = 0
        j = 0
        for zet in self.vrsta:
            if zet.barva == barva:
                i += 1
            elif zet.barva == None:
                j += 1
        return (i, j, 3-i-j)


    def mozno_narediti(self, faza, barva):
        '''V prvi fazi vrne polje, na katerem je mogoče ustvariti mlin.
           V drugi fazi vrne par, v katerem je polje, na katerega je treba
           dati žeton, in ta žeton. (polje, žeton)
           '''
        n = self.preveri_barvo(barva)
        if n[0] == 2 and n[1] == 1:
            for x in self.vrsta:
                if x.barva == None:
                    polje = x
            if faza == 1:
                return polje
            else:
                for x in polje.povezave:
                    if x.barva == barva:
                        return (zeton, x)
                return False
        return False


class Plosca:

## Poimenovanje polj     
##  11|--|--|41|--|--|71
##  --|22|--|42|--|62|--
##  --|--|33|43|53|--|--
##  14|26|34|--|54|64|74
##  --|--|35|45|55|--|--
##  --|26|--|46|--|66|--
##  17|--|--|47|--|--|77

    
    def __init__(self):
        self.polozaji = {(1,1),(1,4),(1,7),
                         (2,2),(2,4),(2,6),
                         (3,3),(3,4),(3,5),
                         (4,1),(4,2),(4,3),
                         (4,5),(4,6),(4,7),
                         (5,3),(5,4),(5,5),
                         (6,2),(6,4),(6,6),
                         (7,1),(7,4),(7,7)}
        self.mlin_x = [[(1,1),(1,4),(1,7)], #vodoravni
                       [(2,2),(2,4),(2,6)],
                       [(3,3),(3,4),(3,5)],
                       [(4,1),(4,2),(4,3)],
                       [(4,5),(4,6),(4,7)],
                       [(5,3),(5,4),(5,5)],
                       [(6,2),(6,4),(6,6)],
                       [(7,1),(7,4),(7,7)]]
        self.mlin_y = [[(1,1),(4,1),(7,1)],#navpicni
                       [(2,2),(4,2),(6,2)],
                       [(3,3),(4,3),(5,3)],
                       [(1,4),(2,4),(3,4)],
                       [(5,4),(6,4),(7,4)],
                       [(3,5),(4,5),(5,5)],
                       [(2,6),(4,6),(6,6)],
                       [(1,7),(4,7),(7,7)]]

                      
        self.polja = dict()
        for x in self.polozaji:
            self.polja[x] = Polje(x)

        self.mlini = []
        for m in self.mlin_x: # po poljih, ki so v mlinih <->
            n = []
            for x in m:
                n.append(self.polja[x])
            self.mlini.append(Mlin(n))
        for m in self.mlin_y: # po poljih, ki so v mlinih |
            n = []
            for y in m:
                n.append(self.polja[y])
            self.mlini.append(Mlin(n))
        

    def naredi_povezave(self):
        '''Ustvari povezave med polji.'''
        for a in self.polja:
            # Povezave   
            p = self.polja[a] #polje, ki ga trenutno gledamo
            
            dx = max(abs(a[1] - 4),1)
            dy = max(abs(a[0] - 4),1)
            
            p.povezi(self.polja.get((a[0],a[1]+dy)))
            p.povezi(self.polja.get((a[0],a[1]-dy)))
            p.povezi(self.polja.get((a[0]-dx,a[1])))
            p.povezi(self.polja.get((a[0]+dx,a[1])))
            
            # Mlini
            for x in self.mlini:
                if p in x.vrsta:
                    p.mlin.append(x)



    def poteze(self, barva_1, barva_2):
        '''Vrne slovar, kjer so vsa polja ločena glede na lastnika oz. praznost.'''
        P = dict()
        P[barva_1] = []
        P[barva_2] = []
        P[None] = []
        for x in self.polja:
            zet = self.polja[x]
            if zet.barva == barva_1:
                P[barva_1].append((zet, None))
            elif zet.barva == barva_2:
                P[barva_2].append((zet, None))
            elif zet.barva == None:
                P[None].append((zet, None))
        return P
        


    





